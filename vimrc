set t_Co=256
colorscheme iceberg

set nobackup
set nowb
set noswapfile

set expandtab
set smarttab

set shiftwidth=4
set tabstop=4

set ai
set si

if has('mouse')
  set mouse=a
endif

set incsearch
set hlsearch

set showmatch
set mat=2

set nu

syntax on

inoremap jk <ESC>

let mapleader = "\<Space>"
